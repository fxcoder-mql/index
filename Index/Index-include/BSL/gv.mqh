/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс глобальной переменной. Better Standard Library. © FXcoder

//todo: переименовать в CBGlVar или CBGlobalVar или CBGlobalVariable для лучшей читаемости
//todo: убрать (bool) у GlobalVariableDel в релизе после 5.2155b, т.к. будет неактуально

#property strict

#include "type/uncopyable.mqh"
#include "util/str.mqh"

class CBGV: public CBUncopyable
{
private:

	string name_;
	int loop_index_;
	int loop_total_;


public:

	void CBGV():
		name_(""),
		loop_index_(-1), loop_total_(0)
	{
	}

	void CBGV(string name):
		name_(name),
		loop_index_(-1), loop_total_(0)
	{
	}

	void CBGV(int index):
		name_(""),
		loop_index_(-1), loop_total_(0)
	{
		name_ = ::GlobalVariableName(index);
	}

	// Изменить имя переменной, к которой будет осуществляться доступ.
	CBGV *name(string name)
	{
		name_ = name;
		return(&this);
	}

	//todo: temp: добавить опцию force для принудительного пересоздания
	bool     check ()                   const { return(      ::GlobalVariableCheck(name_)); }       // Проверяет существование глобальной переменной клиентского терминала.
	bool     del   ()                   const { return((bool)::GlobalVariableDel(name_)); }         // Удаляет глобальную переменную клиентского терминала.
	double   get   ()                   const { return(      ::GlobalVariableGet(name_)); }         // Возвращает значение существующей глобальной переменной клиентского терминала.
	bool     get   (double &value)      const { return(      ::GlobalVariableGet(name_, value)); }  // Возвращает значение существующей глобальной переменной клиентского терминала.
	string   name  ()                   const { return(name_); }                                    // Возвращает имя переменной. Имя указывается при создании, GlobalVariableName здесь не нужен, как и прочие общие функции.
	datetime set   (double value)       const { return(      ::GlobalVariableSet(name_, value)); }  // Устанавливает новое значение глобальной переменной.
	bool     temp  ()                   const { return(      ::GlobalVariableTemp(name_)); }        // Производит попытку создания временной глобальной переменной.
	datetime time  ()                   const { return(      ::GlobalVariableTime(name_)); }        // Возвращает время последнего доступа к глобальной переменной.

	// Если в ГП установлено значение check_value, то установить новое value. Атомарная операция (с блокировкой).
	// Например, установить флаг занятости: if (!(set_on_condition(1, 0)) Print("Занято!");
	bool set_on_condition(double value, double check_value) const { return(::GlobalVariableSetOnCondition(name_, value, check_value)); } // Устанавливает новое значение существующей глобальной переменной, если текущее значение переменной равно значению третьего параметра check_value.

	// Доп. функции

	// Установить значение value, если не 0, иначе удалить
	bool set_del_zero(double value)
	{
		return(set_del(value != 0, value));
	}

	// Установить значение value, если condition=true, иначе удалить
	bool set_del(bool condition, double value = 1.0)
	{
		return(condition ? set(value) > 0 : del());
	}

	//todo: тест с прерыванием и продолжением, повторным проходом
	// Для цикла типа for(CBGV gv; gv.loop();)
	bool loop()
	{
		if (loop_index_ < 0)
			loop_total_ = ::GlobalVariablesTotal();
	
		loop_index_++;
		
		if (loop_index_ >= loop_total_)
		{
			loop_index_ = -1;
			return(false);
		}
		
		name_ = ::GlobalVariableName(loop_index_);
		return(true);
	}

	// Предполагается, что ГП не может иметь пустое имя
	// Для цикла типа for(CBGV gv; gv.loop(prefix);)
	bool loop(string prefix)
	{
		while (loop())
		{
			if (_str.starts_with(name_, prefix))
				return(true);
		}
		
		return(false);
	}

	//todo: test
	bool loop_back()
	{
		if (loop_index_ < 0)
		{
			loop_total_ = ::GlobalVariablesTotal();
			loop_index_ = loop_total_;
		}
	
		loop_index_--;
		
		if (loop_index_ < 0)
		{
			loop_index_ = loop_total_;
			return(false);
		}
		
		name_ = ::GlobalVariableName(loop_index_);
		return(true);
	}

	//todo: test
	bool loop_back(string prefix)
	{
		while (loop_back())
		{
			if (_str.starts_with(name_, prefix))
				return(true);
		}
		
		return(false);
	}

};
