/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива datetime. Better Standard Library. © FXcoder

#property strict

#include "../../util/arr.mqh"
#include "arraynumat.mqh"


class CBArrayTime: public CBArrayNumAT<datetime>
{
public:

	datetime data[];


public:

	void CBArrayTime():                      CBArrayNumAT() { }
	void CBArrayTime(int size):              CBArrayNumAT() { resize(size);          }
	void CBArrayTime(int size, int reserve): CBArrayNumAT() { resize(size, reserve); }

	/* Implementation */
	
	virtual int size()                        override const { return(::ArraySize(data)); }
	virtual int resize(int size)              override       { return(::ArrayResize(data, size, reserve_)); };
	virtual int resize(int size, int reserve) override       { reserve_ = reserve; return(resize(size)); }

	virtual datetime operator[](int i) const override { return(data[i]); } // не использовать, если важна скорость

	virtual int index_of(datetime value, int starting_from = 0) const override { return(_arr.index_of(data, value, starting_from)); }

	virtual void fill(datetime value) override { _arr.fill(data, value); }
	virtual void fill(datetime value, int first, int count) override { _arr.fill(data, value, first, count); }
	virtual int  add (datetime value) override { return(_arr.add(data, value, reserve_)); }

	virtual void clone(const datetime &src[]) override { _arr.clone(data, src); }
	virtual int  copy (const datetime &src[]) override { return(::ArrayCopy(data, src)); }
	virtual int  copy (const datetime &src[], int dst_start, int src_start, int count) override { return(::ArrayCopy(data, src, dst_start, src_start, count)); }

	virtual int      max_index(int first, int count) const override { return(_arr.max_index(data, first, count)); }
	virtual int      min_index(int first, int count) const override { return(_arr.min_index(data, first, count)); }
	virtual datetime max(int first, int count) const override { return(_math.max(data, first, count)); }
	virtual datetime min(int first, int count) const override { return(_math.min(data, first, count)); }

	virtual int      max_index() const override { return(_arr.max_index(data)); }
	virtual int      min_index() const override { return(_arr.min_index(data)); }
	virtual datetime max() const override { return(_math.max(data)); }
	virtual datetime min() const override { return(_math.min(data)); }

	virtual string to_string(string separator) const override { return(_arr.to_string(data, separator, TIME_DATE | TIME_MINUTES)); }

	/* Type-dependent functions */

	string to_string(string separator, int flags) const { return(_arr.to_string(data, separator, flags)); }

};
