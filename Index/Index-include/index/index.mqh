/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс индекса. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../hist/synchistory.mqh"
#include "element.mqh"
#include "formula.mqh"
#include "context.mqh"


class CIndex: public CBUncopyable
{
protected:

	const CIndexContext  *context_;
	
	bool is_initialized_;
	bool is_event_;
	bool is_log_;
	CIndexFormula *formula_;
	string calc_currs_[];


public:

	void CIndex(string gv_prefix, string calc_currs, const string &index_aliases[]):
		context_(NULL),
		formula_(NULL),
		is_initialized_(false),
		is_event_(false),
		is_log_(false)
	{
		context_ = new CIndexContext(gv_prefix, calc_currs, index_aliases);
		formula_ = new CIndexFormula(context_);
	}

	void ~CIndex()
	{
		_ptr.safe_delete(formula_);
		_ptr.safe_delete(context_);
	}

	_GET(CIndexFormula*, formula)
	_GET(bool, is_initialized)
	_GET(bool, is_event)
	_GETARR(string, calc_currs)
	
	// true if formula is correct
	bool init_formula(string formula_string)
	{
		/*
		1. Получить формулу из ГП или разобрать из строки в виде двух массивов для символов и множителей.
		2. Проверить элементы формулы, привести к мажорам и т.п.
		*/

		is_initialized_ = false;
		is_event_ = false;
		
		if (_str.is_int(formula_string))
		{
			long n = StringToInteger(formula_string);
			ASSERT_RETURN(_math.is_in_range(n, (long)0, (long)USHORT_MAX), false);
			
			is_event_ = true;
			
			ASSERT_RETURN(read_gv_formula((ushort)n), false);

			//todo: флаг для логарифма в ГП?
			// номерные формулы всегда с логарифмом
			is_log_ = true;
		}
		else
		{
			ASSERT_RETURN(read_string_formula(formula_string), false);
		}
		
		ASSERT_RETURN(compact_formula(), false);

//LOG(formula_.to_string(4));
//LOG("calc currs: [" + (string)ArraySize(calc_currs_) + "]" + _arr.to_string(calc_currs_, ", "));
		
		is_initialized_ = true;
		return(true);
	}

	// Размер доступной истории и массив результата должны быть достаточного размера заранее и не as series.
	bool calc(const CSyncHistory &hist, int hist_start, int count, int values_start, double &values[])
	{
		if (!is_initialized_)
		{
			_debug.warning("!is_initialized_");
			return(false);
		}

		if (hist.bars_count() < 1)
		{
			_debug.warning("hist.bars_count() < 1");
			return(false);
		}

//LOG("calc:" + VAR(hist_start) + VAR(count) + VAR(values_start));
//LOG(VAR(formula_.length()) + VAR(is_log_) + VAR(count) + VAR(values_start) + VAR(_chart.bars_count()));

		// Подготовить масссив-результат
		ArrayFill(values, values_start, count, is_log_ ? 0.0 : 1.0);

		// Вычислить кривую по формуле индекса
		for (int s = formula_.length() - 1; s >= 0; s--)
		{
			CIndexFormulaElement *element = formula_[s];

			int sym_pos = hist.symbol_index(element.symbol_original());
			if (sym_pos < 0)
			{
				_debug.warning("sym_pos < 0" + VAR(element.symbol()));
				return(false);
			}

			if (is_log_)
			{
				for (int i = 0; i < count; i++)
					values[values_start + i] += element.factor() * log(hist[sym_pos][hist_start + i].close);
			}
			else
			{
				for (int i = 0; i < count; i++)
					values[values_start + i] *= pow(hist[sym_pos][hist_start + i].close, element.factor());
			}
		}

		return(true);
	}
	
	// Актуально только для формул, где все элементы реальны (не индексы, например), что должно быть
	// так после вызова compact_formula().
	// Функция может быть нужна для определения списка символов для загрузки истории.
	int get_symbols(string &symbols[])
	{
		_arr.resize(symbols, 0);
		
		for (int i = formula_.length() - 1; i >= 0; i--)
			_arr.set(symbols, formula_[i].symbol_original());
			
		return(ArraySize(symbols));
	}

	string replace_patterns(string formula_string)
	{
		formula_string = _str.replace(formula_string, "[0]", _Symbol);
		
		if (_symbol.is_forex())
		{
			formula_string = _str.replace(formula_string, "![1]", "!" + _symbol.currency1());
			formula_string = _str.replace(formula_string, "![2]", "!" + _symbol.currency2());

			formula_string = _str.replace(formula_string, "[1]", "!" + _symbol.currency1());
			formula_string = _str.replace(formula_string, "[2]", "!" + _symbol.currency2());
		}
		else
		{
			string symbol = _Symbol;
			
			// aliases
			for (int i = 0, a_count = context_.aliases.size(); i < a_count; i++)
			{
				if (context_.aliases.value(i).name() == _Symbol) // не обязательно приводить к одному регистру
				{
					symbol = context_.aliases.key(i);
					break;
				}
			}
			
			// simple but frequent case: XXXUSD
			if ((StringLen(_Symbol) == 6) && (StringSubstr(_Symbol, 3, 3) == "USD"))
			{
				symbol = StringSubstr(_Symbol, 0, 3);
			}
			
			formula_string = _str.replace(formula_string, "![1]", "!" + symbol);
			formula_string = _str.replace(formula_string, "![2]", "!USD");
			
			formula_string = _str.replace(formula_string, "[1]", "!" + symbol);
			formula_string = _str.replace(formula_string, "[2]", "!USD");
		}

		return(formula_string);
	}


private:

	// Получить формулы из ГП.
	bool read_gv_formula(ushort event_n)
	{
		formula_.clear();
		_arr.clone(calc_currs_, context_.init_calc_currs);

		string prefix = context_.gv_formula_prefix + (string)event_n + ".";
		int prefix_len = StringLen(prefix);
		
		for (CBGV gv; gv.loop(prefix); )
		{
			CIndexFormulaElement element(StringSubstr(gv.name(), prefix_len), gv.get(), context_);
			
			if (!element.is_valid())
				return(false);
			
			if (!update_calc_currs_by_element(element))
				return(false);
			
			formula_.add(element);
		}
		
		return(true); //todo: formula_.length() > 0? пустая формула - это нормально? ведь есть вырожденные случаи, которые выдают 1 или 0
	}

	// Формат: (+|-)Factor1*Symbol1(+|-)Factor2*Symbol2...
	// Альтернатива: (+|-)Symbol1*Factor1(+|-)Symbol2...
	// Множитель может быть до и после инструмента, либо отсутствовать (подразумевая +/-1)
	bool read_string_formula(string formula_string)
	{
		formula_.clear();
		_arr.clone(calc_currs_, context_.init_calc_currs);

		/* Подготовить текст формулы для расшифровки */

		formula_string = _str.upper(_str.trim(_str.replace(formula_string, " ", "")));
		
		// log: + k1*sym1 - k2*sym2...    *+-
		// not: * sym1^k1 / sym2^k2...    */^
		
		bool contains_plus_minus = _str.contains(formula_string, "+") || _str.contains(formula_string, "-");
		bool contains_slash_pow  = _str.contains(formula_string, "/") || _str.contains(formula_string, "^");

		if (contains_plus_minus && contains_slash_pow)
		{
			_debug.warning("Unsupported combination of operators +- with /^ in: " + formula_string);
			return(false);
		}
		
		is_log_ = contains_plus_minus;
		
		uchar add_char = is_log_ ? '+' : '*';
		uchar sub_char = is_log_ ? '-' : '/';
		uchar wt_char  = is_log_ ? '*' : '^';

		string add_str = CharToString(add_char);
		string wt_str  = CharToString(wt_char);

		//todo: изменить поведение?
		// Если ничего не указано, считать текущим символом
		if (formula_string == "")
			formula_string = _Symbol;
		
		// Реверс инструмента определяется знаком `-` перед ним.
		// Если ничего нет, значит нужно брать в прямом порядке, добавить '+' для упрощения разбора формулы.
		ushort ch = StringGetCharacter(formula_string, 0);
	
		if ((ch != add_char) && (ch != sub_char))
			formula_string = add_str + formula_string;
			
		formula_string += add_str;
		
		// специальные обозначения
		formula_string = replace_patterns(formula_string);


		/* Расшифровать формулу (разложить на элементы). В процессе составить единый список валют для вычисления индексов. */
	
		string symbol = ""; // элемент формулы: отдельный символ или символ с множителем (спереди или сзади)
		double factor = 1;
		
		for (int i = 0, len = StringLen(formula_string); i < len; i++)
		{
			ch = StringGetCharacter(formula_string, i);
			
			// начало/конец инструмента/индекса
			if ((ch == add_char) || (ch == sub_char))
			{
				// Конец символа? Записать. Иначе это начало формулы, ничего не делать.
				if (symbol != "")
				{
					// Определить символ и множитель
					int wt_pos = StringFind(symbol, wt_str);
					bool is_valid = false;
					
					// Есть знак умножения
					if (wt_pos != -1)
					{
						string left_part  = StringSubstr(symbol, 0, wt_pos);
						string right_part = StringSubstr(symbol, wt_pos + 1);
						
						CIndexFormulaElement el_left(left_part, 1, context_);
						CIndexFormulaElement el_right(right_part, 1, context_);
						
						bool is_symbol_left  = el_left.is_valid();
						bool is_symbol_right = el_right.is_valid();
						
						// один из множителей должен быть числом, а второй - не числом
						// в нелог. режиме символ может быть только слева
						if ((is_symbol_left == is_symbol_right) || (is_symbol_right && !is_log_))
						{
							_debug.warning("Error in formula: " + formula_string + " [... " + symbol + " ...]" +
								VAR(is_log_) + VAR(is_symbol_left) + VAR(is_symbol_right) + VAR(left_part) + VAR(right_part));
							return(false);
						}
						else if (is_symbol_left)
						{
							is_valid = true;
							symbol = left_part;
							factor *= StringToDouble(right_part);
						}
						else // if (is_symbol_left && is_log_)
						{
							is_valid = true;
							symbol = right_part;
							factor *= StringToDouble(left_part);
						}
					}
					else
					{
						CIndexFormulaElement el(symbol, 1, context_);
						is_valid = el.is_valid();
					}
					
					if (!is_valid)
					{
						_debug.warning("!is_valid: " + formula_string + " [... " + symbol + " ...]");
						return(false);
					}
					
					// Добавить символ с множителем в формулу
					CIndexFormulaElement *element = formula_[formula_.add(symbol, factor)];
					
					if (!update_calc_currs_by_element(element))
						return(false);
					
					symbol = "";
				}
				
				//  следующее направление
				factor = (ch == add_char) ? 1 : -1;
			}
			else
			{
				symbol = symbol + ShortToString(ch);
			}
		}

		return(true);
	}

	// Привести всё к парам с USD и убрать элементы с нулевыми множителями.
	bool compact_formula()
	{
		/* Сделать проход с разбором индексов и кроссов на кроссы с USD */
		
		CIndexFormula compact_formula(context_);
	
		for (int i = 0, len = formula_.length(); i < len; i++)
		{
			CIndexFormulaElement *element = formula_[i];
			
			if (element.is_index())
			{
				// Получить формулу для индекса
				CIndexFormula index_formula(context_);
				
				if (!get_index_formula(element, index_formula))
				{
					_debug.warning("!get_index_formula");
					return(false);
				}
	
				// добавить формулу в общую формулу
				compact_formula.add(index_formula, element.factor());
			}
			else if (element.is_pair())
			{
				CBSymbol *av_sym = element.av_sym();
				string curr1 = av_sym.currency1();
				string curr2 = av_sym.currency2();
				
				if ((curr1 == "USD") || (curr2 == "USD"))
				{
					// кросс с USD, просто добавить
					compact_formula.add(element);
				}
				else
				{
					// Разбить на кроссы с USD
					string usd_pair_symbol;
					double usd_pair_factor;
					
					if (!get_curr_usd_pair(curr1, usd_pair_symbol, usd_pair_factor))
						return(false);
	
					compact_formula.add(usd_pair_symbol, -usd_pair_factor * element.factor());
					
	
					if (!get_curr_usd_pair(curr2, usd_pair_symbol, usd_pair_factor))
						return(false);
	
					compact_formula.add(usd_pair_symbol, usd_pair_factor * element.factor());
				}
			}
			else
			{
				// Специальный символ (символ-кросс с USD, но не имеющий его в обозначении), например SP500, GOLD, _NG
				compact_formula.add(element);
			}
		}
	

		/* Убрать символы с нулевой степенью (добавить в результат только ненулевые) */
		
		formula_.clear();
		
		for (int i = 0, count = compact_formula.length(); i < count; i++)
		{
			if (!_math.is_zero(compact_formula[i].factor()))
				formula_.add(compact_formula[i]);
		}
		
		return(true);
	}

	bool update_calc_currs_by_element(const CIndexFormulaElement &element)
	{
		// Добавить валюты символа в список валют для расчёта индексов
		if (element.is_pair())
		{
			// валютная пара
			CBSymbol *av_sym = element.av_sym();
			_arr.set(calc_currs_, av_sym.currency1());
			_arr.set(calc_currs_, av_sym.currency2());
		}
		else if (element.is_available())
		{
			// доступен в обзоре рынке, но не пара
			_arr.set(calc_currs_, element.symbol());
			_arr.set(calc_currs_, "USD");
		}
		else if (element.is_index())
		{
			string alias_index = context_.symbol_to_index_alias(element.index_name());
			_arr.set(calc_currs_, alias_index == "" ? element.index_name() : alias_index);
		}
		else
		{
			// один из вариантов выше обязан сработать
			_debug.warning(__FUNCTION__ + ". Unreachable code. " + VAR(element.symbol()));
			return(false);
		}
		
		return(true);
	}

	// Получить мажор для валюты
	bool get_curr_usd_pair(string curr, string &symbol, double &factor)
	{
		for (int i = context_.av_syms.size() - 1; i >= 0; i--)
		{
			CBSymbol *av_sym = context_.av_syms[i];
			if (!av_sym.is_forex())
				continue;
			
			string curr1 = av_sym.currency1();
			string curr2 = av_sym.currency2();
			
			if ((curr1 == "USD") && (curr2 == curr))
			{
				symbol = _str.upper(av_sym.name());
				factor = 1;
				return(true);
			}
			
			if ((curr2 == "USD") && (curr1 == curr))
			{
				symbol = _str.upper(av_sym.name());
				factor = -1;
				return(true);
			}
		}

		return(false);
	}
	
	// Найти правильное название кросса с USD.
	// USD/C считается прямым кроссом, factor = 1
	// C/USD и CFD дают factor = -1
	bool get_index_usd_pair(string index, string &symbol, double &factor)
	{
		if (index == "USD")
		{
			symbol = ""; // USDUSD
			factor = 1; //todo: 0?
			return(true);
		}
		
		// search in aliases
		symbol = context_.index_alias_to_symbol(index);

		if (symbol != "")
		{
			factor = -1;
			return(true);
		}

		// search forex pair
		if (get_curr_usd_pair(index, symbol, factor))
			return(true);

		// CFD и специальные инструменты

		for (int i = context_.av_syms.size() - 1; i >= 0; i--)
		{
			CBSymbol* sym = context_.av_syms[i];
			string sym_name = _str.upper(sym.name());
			
			if ((sym_name == index) ||                     // IBM, MMM...
				(_str.contains(sym_name, index + "USD")))  // XAU, BTC...
			{
				symbol = sym_name;
				factor = -1;
				return(true);
			}
		}

		return(false);
	}

	// Получить формулу индекса.
	bool get_index_formula(const CIndexFormulaElement &element, CIndexFormula &formula)
	{
		formula.clear();
	
		// количество валют для расчета
		int cc_count = ArraySize(calc_currs_);
	
		// Индекс USD (произведение Ci/USD)
		for (int i = 0; i < cc_count; i++)
		{
			string symbol;
			double factor;
			
			if (!get_index_usd_pair(calc_currs_[i], symbol, factor))
			{
				_debug.warning("!get_index_usd_pair #1: " + calc_currs_[i]);
				return(false);
			}
			
			// ignore "USD"
			if (symbol != "")
				formula.add(symbol, factor / cc_count);
		}
		
		string index = element.index_name();
		
		// index = C/USD
		if (index != "USD")
		{
			string symbol;
			double factor;
			
			if (!get_index_usd_pair(index, symbol, factor))
			{
				_debug.warning("!get_index_usd_pair #2: " + index);
				return(false);
			}
				
			formula.add(symbol, -factor); // -factor, т.к. find_usd_cross даёт -1 для C/USD
		}
	
		return(true);
	}

};
